script_name('HelperLovli')
script_author('by lovell fam')
script_version("28.07.2020")
local samp = require 'lib.samp.events'
local vector = require 'vector3d'
local tpt, synct = false, false
local tpCountt, timert = 0, 0
local ffi = require("ffi")
local request = require('lib.requests')
local encoding = require 'encoding'
local version = '4.0'
local servak = 'by lovell fam'
local servakautoenter = '8869'
local Lovlya1 = false
local allFams = {"Family", "Dynasty", "Corporation", "Squad", "Crew", "Empire", "Brotherhood"}
local inicfg = require 'inicfg'
local vkeys = require 'vkeys'
local imgui = require 'imgui'
local mem = require 'memory'
local bNotf, notf = pcall(import, "imgui_notf.lua")
local time = nil
local captime = 0
local hfind = 0
local forFind = {}
local houses = {}
local t = 0
local payday = false
local captcha = ''
local captchaTable = {}
local antinefludi = imgui.ImBool(false)
local antinefluditwo = imgui.ImBool(false)
local avtonazatie = imgui.ImBool(false)
local avtonazatietwo = imgui.ImBool(false)
local aenter = imgui.ImBool(false)
local oopsTimer = imgui.ImBool(false)
local delnoflood = imgui.ImBool(false)
local bindsbiv = imgui.ImBool(false)
local perebind = imgui.ImBool(false)
waitavtonazatie = imgui.ImInt(0)
waitavtonazatietwo = imgui.ImInt(0)
waitaenter = imgui.ImInt(0)

local HLcfg = inicfg.load({
    main = {
        activation = 'lovell',
        nClear = false,
        jtext = 'ez',
        resetpd = false,
        utime = false,
        ptime = false,
        HLmsg = false,
        max5 = false,
		clearchat = false,
		perebind = false,
		bindstyle = false,
		delnoflood = false,
		bindsbiv = false,
		bindphone = false,
		bindlock = false,
        plus3click = false,
        floodN = false,
        autofind = false,
        onkey = false,
        key = 'U',
        texttime = false,
        theme = 1,
   oopsTimer = true
    }
}, "HelperLovli")

--         autoenter = false,
-- zero = false

local encoding = require 'encoding'
encoding.default = 'CP1251'
u8 = encoding.UTF8
local avtonazatie = imgui.ImBool(false)
local sync1 = imgui.ImBool(false)
local sync2 = imgui.ImBool(false)
local sync3 = imgui.ImBool(false)
local col = imgui.ImBool(false)
local sync4 = imgui.ImBool(false)
local avtonazatietwo = imgui.ImBool(false)
waitavtonazatie = imgui.ImInt(45)
waitavtonazatietwo = imgui.ImInt(45)
local tags = imgui.ImBool(false)
local main_window_state = imgui.ImBool(false)
buffer = imgui.ImBuffer(tostring(HLcfg.main.jtext), 256)
buffer.v = string.gsub(tostring(buffer.v), '"', '')
keybuff = imgui.ImBuffer(tostring(HLcfg.main.key), 5)
buffermenu = imgui.ImBuffer(tostring(HLcfg.main.activation), 256)

local ffi = require("ffi")

function ShowMessage(text, title, style)
    ffi.cdef [[
        int MessageBoxA(
            void* hWnd,
            const char* lpText,
            const char* lpCaption,
            unsigned int uType
        );
    ]]
    local hwnd = ffi.cast('void*', readMemory(0x00C8CF88, 4, false))
    ffi.C.MessageBoxA(hwnd, text,  title, style and (style + 0x50000) or 0x50000)
end

function texttimer(text)
    if text == "" or text == " " then
        mainIni = inicfg.load(nil, directIni)
        if mainIni.config.active == true then
        mainIni.config.active = false
            if inicfg.save(mainIni, directIni) then
                sampAddChatMessage("{DA70D6}[�������� �����]{FFFFFF} ����� ��� ����� ��������.", -1)
            end

        else
        mainIni.config.active = true
            if inicfg.save(mainIni, directIni) then
                sampAddChatMessage("{DA70D6}[�������� �����]{FFFFFF} ����� ��� ����� �������.", -1)
            end
        end
    else

        mainIni.config.textlovli = text
        if inicfg.save(mainIni, directIni) then
            sampAddChatMessage("{DA70D6}[�������� �����]{FFFFFF} ����� ��� ����� ���������� �� "..mainIni.config.textlovli.."", 0xFFFFFF)
        end
    end
end

function nkaptcha()
    mainIni = inicfg.load(nil, directIni)
        if mainIni.config.nkaptcha == true then
        mainIni.config.nkaptcha = false
            if inicfg.save(mainIni, directIni) then
                sampAddChatMessage("{DA70D6}[�������� �����]{FFFFFF} � � ������� � ����� ��� ����� ���������.", -1)
            end

        else
        mainIni.config.nkaptcha = true
            if inicfg.save(mainIni, directIni) then
                sampAddChatMessage("{DA70D6}[�������� �����]{FFFFFF} � � ������� � ����� ��� ����� ��������.", -1)
            end
        end
end

function samp.onServerMessage(color, text)
    if string.find(text, '������ ���� ��� ���', 1, true) then
        mainIni = inicfg.load(nil, directIni)
        if mainIni.config.active == true then
        sampAddChatMessage("{73B461}[����������] {FFFFFF}����������! ������ ���� ��� ���!",-1)
        sampSendChat(mainIni.config.textlovli)
        end
    end
    if string.find(text, "������ ���� ������ ���") then
        mainIni = inicfg.load(nil, directIni)
        if mainIni.config.active == true then
        sampAddChatMessage("{73B461}[����������] {FFFFFF}����������! ������ ���� ������ ���!",-1)
        sampSendChat(mainIni.config.textlovli)
        end
    end
    if string.find(text, "__________________________________") then
        startpd = os.clock()
        reset()
    end
    if string.find(text, "��� ��������� PayDay �� ������ �������� ������� 20 �����.") then
        startpd = os.clock()
        reset()
    end
end


function reset()
    lua_thread.create(function ()
        wait(5000)
        startpd = nil
        startslet = nil
    end)
end

function toSendGet(str)
    local diff = urlencode(u8:encode(str, 'CP1251'))
    return diff
end

function urlencode(str)
    if (str) then
        str = string.gsub (str, "\n", "\r\n")
        str = string.gsub (str, "([^%w ])",
            function (c) return string.format ("%%%02X", string.byte(c)) end)
        str = string.gsub (str, " ", "+")
    end
    return str
end

function samp.onCreate3DText(i, clr, pos, distance, ignoreWalls, playerId, vehicleId, textd)
	if antinefluditwo.v then
		lua_thread.create(function()
			repeat wait(0) until textd:find("*** ��� ��������� ***") or textd:find("������ ���������")  or textd:find("*** �������� ��������� ***")
	 if i == 1024 and textd:find("*** ��� ��������� ***") or textd:find("������ ���������") or textd:find("*** �������� ��������� ***") then
		 antinefluditwo.v = false
	 end
 end)
 end
		 if avtonazatietwo.v and (textd:find("*** ��� ��������� ***") or textd:find("������ ���������") or textd:find("*** �������� ��������� ***")) then
			 lua_thread.create(function()
				 antinefludi.v = false
				 antinefluditwo.v = false
			 wait (waitavtonazatietwo.v)
			 local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
					 local data = allocateMemory(68)
					 sampStorePlayerOnfootData(myId, data)
					 setStructElement(data, 36, 1, 128, false)
					 sampSendOnfootData(data)
					 freeMemory(data)
 end)
 end
 end

function samp.onServerMessage(color, text)
if avtonazatie.v and (text:match('��� ��������� PayDay �� ������ �������� ������� 20 �����.') or text:match('__________���������� ���__________')) then
lua_thread.create(function()
	wait(0)
	wait (waitavtonazatie.v)
	local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
			local data = allocateMemory(68)
			sampStorePlayerOnfootData(myId, data)
			setStructElement(data, 36, 1, 128, false)
			sampSendOnfootData(data)
			freeMemory(data)
		end)
	end
end

function getMyName()
    local ok, pid = sampGetPlayerIdByCharHandle(PLAYER_PED)
    if ok then
        return sampGetPlayerNickname(pid)
    else
        return '����������'
    end
end


function resethider()
    lua_thread.create(function ()
        wait(10000)

        hider = false
    end)
end

function apply_custom_style()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    style.WindowRounding = 2
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    style.ChildWindowRounding = 2.0
    style.FrameRounding = 3
    style.ItemSpacing = imgui.ImVec2(5.0, 4.0)
    style.ScrollbarSize = 13.0
    style.ScrollbarRounding = 0
    style.GrabMinSize = 8.0
    style.GrabRounding = 1.0
    style.WindowPadding = imgui.ImVec2(4.0, 4.0)
    style.FramePadding = imgui.ImVec2(3.5, 3.5)
    style.ButtonTextAlign = imgui.ImVec2(0.0, 0.5)
    colors[clr.WindowBg]              = ImVec4(0.14, 0.12, 0.16, 1.00);
    colors[clr.ChildWindowBg]         = ImVec4(0.30, 0.20, 0.39, 0.00);
    colors[clr.PopupBg]               = ImVec4(0.05, 0.05, 0.10, 0.90);
    colors[clr.Border]                = ImVec4(0.89, 0.85, 0.92, 0.30);
    colors[clr.BorderShadow]          = ImVec4(0.00, 0.00, 0.00, 0.00);
    colors[clr.FrameBg]               = ImVec4(0.30, 0.20, 0.39, 1.00);
    colors[clr.FrameBgHovered]        = ImVec4(0.41, 0.19, 0.63, 0.68);
    colors[clr.FrameBgActive]         = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.TitleBg]               = ImVec4(0.41, 0.19, 0.63, 0.45);
    colors[clr.TitleBgCollapsed]      = ImVec4(0.41, 0.19, 0.63, 0.35);
    colors[clr.TitleBgActive]         = ImVec4(0.41, 0.19, 0.63, 0.78);
    colors[clr.MenuBarBg]             = ImVec4(0.30, 0.20, 0.39, 0.57);
    colors[clr.ScrollbarBg]           = ImVec4(0.30, 0.20, 0.39, 1.00);
    colors[clr.ScrollbarGrab]         = ImVec4(0.41, 0.19, 0.63, 0.31);
    colors[clr.ScrollbarGrabHovered]  = ImVec4(0.41, 0.19, 0.63, 0.78);
    colors[clr.ScrollbarGrabActive]   = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.ComboBg]               = ImVec4(0.30, 0.20, 0.39, 1.00);
    colors[clr.CheckMark]             = ImVec4(0.56, 0.61, 1.00, 1.00);
    colors[clr.SliderGrab]            = ImVec4(0.41, 0.19, 0.63, 0.24);
    colors[clr.SliderGrabActive]      = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.Button]                = ImVec4(0.41, 0.19, 0.63, 0.44);
    colors[clr.ButtonHovered]         = ImVec4(0.41, 0.19, 0.63, 0.86);
    colors[clr.ButtonActive]          = ImVec4(0.64, 0.33, 0.94, 1.00);
    colors[clr.Header]                = ImVec4(0.41, 0.19, 0.63, 0.76);
    colors[clr.HeaderHovered]         = ImVec4(0.41, 0.19, 0.63, 0.86);
    colors[clr.HeaderActive]          = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.ResizeGrip]            = ImVec4(0.41, 0.19, 0.63, 0.20);
    colors[clr.ResizeGripHovered]     = ImVec4(0.41, 0.19, 0.63, 0.78);
    colors[clr.ResizeGripActive]      = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.CloseButton]           = ImVec4(1.00, 1.00, 1.00, 0.75);
    colors[clr.CloseButtonHovered]    = ImVec4(0.88, 0.74, 1.00, 0.59);
    colors[clr.CloseButtonActive]     = ImVec4(0.88, 0.85, 0.92, 1.00);
    colors[clr.PlotLines]             = ImVec4(0.89, 0.85, 0.92, 0.63);
    colors[clr.PlotLinesHovered]      = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.PlotHistogram]         = ImVec4(0.89, 0.85, 0.92, 0.63);
    colors[clr.PlotHistogramHovered]  = ImVec4(0.41, 0.19, 0.63, 1.00);
    colors[clr.TextSelectedBg]        = ImVec4(0.41, 0.19, 0.63, 0.43);
    colors[clr.ModalWindowDarkening]  = ImVec4(0.20, 0.20, 0.20, 0.35);
end

function lightBlue()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4



    colors[clr.Text]   = ImVec4(0.00, 0.00, 0.00, 0.51)
    colors[clr.TextDisabled]   = ImVec4(0.24, 0.24, 0.24, 1.00)
    colors[clr.WindowBg]              = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.ChildWindowBg]         = ImVec4(0.96, 0.96, 0.96, 1.00)
    colors[clr.PopupBg]               = ImVec4(0.92, 0.92, 0.92, 1.00)
    colors[clr.Border]                = ImVec4(0.86, 0.86, 0.86, 1.00)
    colors[clr.BorderShadow]          = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.FrameBg]               = ImVec4(0.88, 0.88, 0.88, 1.00)
    colors[clr.FrameBgHovered]        = ImVec4(0.82, 0.82, 0.82, 1.00)
    colors[clr.FrameBgActive]         = ImVec4(0.76, 0.76, 0.76, 1.00)
    colors[clr.TitleBg]               = ImVec4(0.00, 0.45, 1.00, 0.82)
    colors[clr.TitleBgCollapsed]      = ImVec4(0.00, 0.45, 1.00, 0.82)
    colors[clr.TitleBgActive]         = ImVec4(0.00, 0.45, 1.00, 0.82)
    colors[clr.MenuBarBg]             = ImVec4(0.00, 0.37, 0.78, 1.00)
    colors[clr.ScrollbarBg]           = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.ScrollbarGrab]         = ImVec4(0.00, 0.35, 1.00, 0.78)
    colors[clr.ScrollbarGrabHovered]  = ImVec4(0.00, 0.33, 1.00, 0.84)
    colors[clr.ScrollbarGrabActive]   = ImVec4(0.00, 0.31, 1.00, 0.88)
    colors[clr.ComboBg]               = ImVec4(0.92, 0.92, 0.92, 1.00)
    colors[clr.CheckMark]             = ImVec4(0.00, 0.49, 1.00, 0.59)
    colors[clr.SliderGrab]            = ImVec4(0.00, 0.49, 1.00, 0.59)
    colors[clr.SliderGrabActive]      = ImVec4(0.00, 0.39, 1.00, 0.71)
    colors[clr.Button]                = ImVec4(0.00, 0.49, 1.00, 0.59)
    colors[clr.ButtonHovered]         = ImVec4(0.00, 0.49, 1.00, 0.71)
    colors[clr.ButtonActive]          = ImVec4(0.00, 0.49, 1.00, 0.78)
    colors[clr.Header]                = ImVec4(0.00, 0.49, 1.00, 0.78)
    colors[clr.HeaderHovered]         = ImVec4(0.00, 0.49, 1.00, 0.71)
    colors[clr.HeaderActive]          = ImVec4(0.00, 0.49, 1.00, 0.78)
    colors[clr.ResizeGrip]            = ImVec4(0.00, 0.39, 1.00, 0.59)
    colors[clr.ResizeGripHovered]     = ImVec4(0.00, 0.27, 1.00, 0.59)
    colors[clr.ResizeGripActive]      = ImVec4(0.00, 0.25, 1.00, 0.63)
    colors[clr.CloseButton]           = ImVec4(0.00, 0.35, 0.96, 0.71)
    colors[clr.CloseButtonHovered]    = ImVec4(0.00, 0.31, 0.88, 0.69)
    colors[clr.CloseButtonActive]     = ImVec4(0.00, 0.25, 0.88, 0.67)
    colors[clr.PlotLines]             = ImVec4(0.00, 0.39, 1.00, 0.75)
    colors[clr.PlotLinesHovered]      = ImVec4(0.00, 0.39, 1.00, 0.75)
    colors[clr.PlotHistogram]         = ImVec4(0.00, 0.39, 1.00, 0.75)
    colors[clr.PlotHistogramHovered]  = ImVec4(0.00, 0.35, 0.92, 0.78)
    colors[clr.TextSelectedBg]        = ImVec4(0.00, 0.47, 1.00, 0.59)
    colors[clr.ModalWindowDarkening]  = ImVec4(0.20, 0.20, 0.20, 0.35)
end

function darkRedTheme()
	imgui.SwitchContext()
 local style = imgui.GetStyle()
 local colors = style.Colors
 local clr = imgui.Col
 local ImVec4 = imgui.ImVec4
 local ImVec2 = imgui.ImVec2

 style.WindowPadding = imgui.ImVec2(8, 8)
 style.WindowRounding = 6
 style.ChildWindowRounding = 5
 style.FramePadding = imgui.ImVec2(5, 3)
 style.FrameRounding = 3.0
 style.ItemSpacing = imgui.ImVec2(5, 4)
 style.ItemInnerSpacing = imgui.ImVec2(4, 4)
 style.IndentSpacing = 21
 style.ScrollbarSize = 10.0
 style.ScrollbarRounding = 13
 style.GrabMinSize = 8
 style.GrabRounding = 1
 style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
 style.ButtonTextAlign = imgui.ImVec2(0.5, 0.5)

 colors[clr.Text]                   = ImVec4(0.95, 0.96, 0.98, 1.00);
 colors[clr.TextDisabled]           = ImVec4(0.29, 0.29, 0.29, 1.00);
 colors[clr.WindowBg]               = ImVec4(0.14, 0.14, 0.14, 1.00);
 colors[clr.ChildWindowBg]          = ImVec4(0.12, 0.12, 0.12, 1.00);
 colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94);
 colors[clr.Border]                 = ImVec4(0.14, 0.14, 0.14, 1.00);
 colors[clr.BorderShadow]           = ImVec4(1.00, 1.00, 1.00, 0.10);
 colors[clr.FrameBg]                = ImVec4(0.22, 0.22, 0.22, 1.00);
 colors[clr.FrameBgHovered]         = ImVec4(0.18, 0.18, 0.18, 1.00);
 colors[clr.FrameBgActive]          = ImVec4(0.09, 0.12, 0.14, 1.00);
 colors[clr.TitleBg]                = ImVec4(0.14, 0.14, 0.14, 0.81);
 colors[clr.TitleBgActive]          = ImVec4(0.14, 0.14, 0.14, 1.00);
 colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51);
 colors[clr.MenuBarBg]              = ImVec4(0.20, 0.20, 0.20, 1.00);
 colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.39);
 colors[clr.ScrollbarGrab]          = ImVec4(0.36, 0.36, 0.36, 1.00);
 colors[clr.ScrollbarGrabHovered]   = ImVec4(0.18, 0.22, 0.25, 1.00);
 colors[clr.ScrollbarGrabActive]    = ImVec4(0.24, 0.24, 0.24, 1.00);
 colors[clr.ComboBg]                = ImVec4(0.24, 0.24, 0.24, 1.00);
 colors[clr.CheckMark]              = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.SliderGrab]             = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.SliderGrabActive]       = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.Button]                 = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.ButtonHovered]          = ImVec4(1.00, 0.39, 0.39, 1.00);
 colors[clr.ButtonActive]           = ImVec4(1.00, 0.21, 0.21, 1.00);
 colors[clr.Header]                 = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.HeaderHovered]          = ImVec4(1.00, 0.39, 0.39, 1.00);
 colors[clr.HeaderActive]           = ImVec4(1.00, 0.21, 0.21, 1.00);
 colors[clr.ResizeGrip]             = ImVec4(1.00, 0.28, 0.28, 1.00);
 colors[clr.ResizeGripHovered]      = ImVec4(1.00, 0.39, 0.39, 1.00);
 colors[clr.ResizeGripActive]       = ImVec4(1.00, 0.19, 0.19, 1.00);
 colors[clr.CloseButton]            = ImVec4(0.40, 0.39, 0.38, 0.16);
 colors[clr.CloseButtonHovered]     = ImVec4(0.40, 0.39, 0.38, 0.39);
 colors[clr.CloseButtonActive]      = ImVec4(0.40, 0.39, 0.38, 1.00);
 colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00);
 colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00);
 colors[clr.PlotHistogram]          = ImVec4(1.00, 0.21, 0.21, 1.00);
 colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.18, 0.18, 1.00);
 colors[clr.TextSelectedBg]         = ImVec4(1.00, 0.32, 0.32, 1.00);
 colors[clr.ModalWindowDarkening]   = ImVec4(0.26, 0.26, 0.26, 0.60);
end

function yellowTheme()
	imgui.SwitchContext()
	local style = imgui.GetStyle()
	local colors = style.Colors
	local clr = imgui.Col
	local ImVec4 = imgui.ImVec4
	local ImVec2 = imgui.ImVec2

	style.WindowRounding         = 4.0
	style.WindowTitleAlign       = ImVec2(0.5, 0.5)
	style.ChildWindowRounding    = 2.0
	style.FrameRounding          = 2.0
	style.ItemSpacing            = ImVec2(10, 5)
	style.ScrollbarSize          = 15
	style.ScrollbarRounding      = 0
	style.GrabMinSize            = 9.6
	style.GrabRounding           = 1.0
	style.WindowPadding          = ImVec2(10, 10)
	style.AntiAliasedLines       = true
	style.AntiAliasedShapes      = true
	style.FramePadding           = ImVec2(5, 4)
	style.DisplayWindowPadding   = ImVec2(27, 27)
	style.DisplaySafeAreaPadding = ImVec2(5, 5)
	style.ButtonTextAlign        = ImVec2(0.5, 0.5)

	colors[clr.Text]                 = ImVec4(0.92, 0.92, 0.92, 1.00)
	colors[clr.TextDisabled]         = ImVec4(0.44, 0.44, 0.44, 1.00)
	colors[clr.WindowBg]             = ImVec4(0.06, 0.06, 0.06, 1.00)
	colors[clr.ChildWindowBg]        = ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.PopupBg]              = ImVec4(0.08, 0.08, 0.08, 0.94)
	colors[clr.ComboBg]              = ImVec4(0.08, 0.08, 0.08, 0.94)
	colors[clr.Border]               = ImVec4(0.51, 0.36, 0.15, 1.00)
	colors[clr.BorderShadow]         = ImVec4(0.00, 0.00, 0.00, 0.00)
	colors[clr.FrameBg]              = ImVec4(0.11, 0.11, 0.11, 1.00)
	colors[clr.FrameBgHovered]       = ImVec4(0.51, 0.36, 0.15, 1.00)
	colors[clr.FrameBgActive]        = ImVec4(0.78, 0.55, 0.21, 1.00)
	colors[clr.TitleBg]              = ImVec4(0.51, 0.36, 0.15, 1.00)
	colors[clr.TitleBgActive]        = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.TitleBgCollapsed]     = ImVec4(0.00, 0.00, 0.00, 0.51)
	colors[clr.MenuBarBg]            = ImVec4(0.11, 0.11, 0.11, 1.00)
	colors[clr.ScrollbarBg]          = ImVec4(0.06, 0.06, 0.06, 0.53)
	colors[clr.ScrollbarGrab]        = ImVec4(0.21, 0.21, 0.21, 1.00)
	colors[clr.ScrollbarGrabHovered] = ImVec4(0.47, 0.47, 0.47, 1.00)
	colors[clr.ScrollbarGrabActive]  = ImVec4(0.81, 0.83, 0.81, 1.00)
	colors[clr.CheckMark]            = ImVec4(0.78, 0.55, 0.21, 1.00)
	colors[clr.SliderGrab]           = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.SliderGrabActive]     = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.Button]               = ImVec4(0.51, 0.36, 0.15, 1.00)
	colors[clr.ButtonHovered]        = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.ButtonActive]         = ImVec4(0.78, 0.55, 0.21, 1.00)
	colors[clr.Header]               = ImVec4(0.51, 0.36, 0.15, 1.00)
	colors[clr.HeaderHovered]        = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.HeaderActive]         = ImVec4(0.93, 0.65, 0.14, 1.00)
	colors[clr.Separator]            = ImVec4(0.21, 0.21, 0.21, 1.00)
	colors[clr.SeparatorHovered]     = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.SeparatorActive]      = ImVec4(0.78, 0.55, 0.21, 1.00)
	colors[clr.ResizeGrip]           = ImVec4(0.21, 0.21, 0.21, 1.00)
	colors[clr.ResizeGripHovered]    = ImVec4(0.91, 0.64, 0.13, 1.00)
	colors[clr.ResizeGripActive]     = ImVec4(0.78, 0.55, 0.21, 1.00)
	colors[clr.CloseButton]          = ImVec4(0.47, 0.47, 0.47, 1.00)
	colors[clr.CloseButtonHovered]   = ImVec4(0.98, 0.39, 0.36, 1.00)
	colors[clr.CloseButtonActive]    = ImVec4(0.98, 0.39, 0.36, 1.00)
	colors[clr.PlotLines]            = ImVec4(0.61, 0.61, 0.61, 1.00)
	colors[clr.PlotLinesHovered]     = ImVec4(1.00, 0.43, 0.35, 1.00)
	colors[clr.PlotHistogram]        = ImVec4(0.90, 0.70, 0.00, 1.00)
	colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
	colors[clr.TextSelectedBg]       = ImVec4(0.26, 0.59, 0.98, 0.35)
	colors[clr.ModalWindowDarkening] = ImVec4(0.80, 0.80, 0.80, 0.35)
end

function blackTheme()
	imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2

     style.WindowPadding = ImVec2(15, 15)
     style.WindowRounding = 15.0
     style.FramePadding = ImVec2(5, 5)
     style.ItemSpacing = ImVec2(12, 8)
     style.ItemInnerSpacing = ImVec2(8, 6)
     style.IndentSpacing = 25.0
     style.ScrollbarSize = 15.0
     style.ScrollbarRounding = 15.0
     style.GrabMinSize = 15.0
     style.GrabRounding = 7.0
     style.ChildWindowRounding = 8.0
     style.FrameRounding = 6.0


       colors[clr.Text] = ImVec4(0.95, 0.96, 0.98, 1.00)
       colors[clr.TextDisabled] = ImVec4(0.36, 0.42, 0.47, 1.00)
       colors[clr.WindowBg] = ImVec4(0.11, 0.15, 0.17, 1.00)
       colors[clr.ChildWindowBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
       colors[clr.Border] = ImVec4(0.43, 0.43, 0.50, 0.50)
       colors[clr.BorderShadow] = ImVec4(0.00, 0.00, 0.00, 0.00)
       colors[clr.FrameBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.FrameBgHovered] = ImVec4(0.12, 0.20, 0.28, 1.00)
       colors[clr.FrameBgActive] = ImVec4(0.09, 0.12, 0.14, 1.00)
       colors[clr.TitleBg] = ImVec4(0.09, 0.12, 0.14, 0.65)
       colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
       colors[clr.TitleBgActive] = ImVec4(0.08, 0.10, 0.12, 1.00)
       colors[clr.MenuBarBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.39)
       colors[clr.ScrollbarGrab] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
       colors[clr.ScrollbarGrabActive] = ImVec4(0.09, 0.21, 0.31, 1.00)
       colors[clr.ComboBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.CheckMark] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrab] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrabActive] = ImVec4(0.37, 0.61, 1.00, 1.00)
       colors[clr.Button] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ButtonHovered] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.ButtonActive] = ImVec4(0.06, 0.53, 0.98, 1.00)
       colors[clr.Header] = ImVec4(0.20, 0.25, 0.29, 0.55)
       colors[clr.HeaderHovered] = ImVec4(0.26, 0.59, 0.98, 0.80)
       colors[clr.HeaderActive] = ImVec4(0.26, 0.59, 0.98, 1.00)
       colors[clr.ResizeGrip] = ImVec4(0.26, 0.59, 0.98, 0.25)
       colors[clr.ResizeGripHovered] = ImVec4(0.26, 0.59, 0.98, 0.67)
       colors[clr.ResizeGripActive] = ImVec4(0.06, 0.05, 0.07, 1.00)
       colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16)
       colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39)
       colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00)
       colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
       colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
       colors[clr.PlotHistogram] = ImVec4(0.90, 0.70, 0.00, 1.00)
       colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
       colors[clr.TextSelectedBg] = ImVec4(0.25, 1.00, 0.00, 0.43)
       colors[clr.ModalWindowDarkening] = ImVec4(1.00, 0.98, 0.95, 0.73)
end

function mintTheme()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4

    style.WindowRounding = 2.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.84)
    style.ChildWindowRounding = 2.0
    style.FrameRounding = 2.0
    style.ItemSpacing = imgui.ImVec2(5.0, 4.0)
    style.ScrollbarSize = 13.0
    style.ScrollbarRounding = 0
    style.GrabMinSize = 8.0
    style.GrabRounding = 1.0

    colors[clr.FrameBg]                = ImVec4(0.16, 0.48, 0.42, 0.54)
    colors[clr.FrameBgHovered]         = ImVec4(0.26, 0.98, 0.85, 0.40)
    colors[clr.FrameBgActive]          = ImVec4(0.26, 0.98, 0.85, 0.67)
    colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
    colors[clr.TitleBgActive]          = ImVec4(0.16, 0.48, 0.42, 1.00)
    colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
    colors[clr.CheckMark]              = ImVec4(0.26, 0.98, 0.85, 1.00)
    colors[clr.SliderGrab]             = ImVec4(0.24, 0.88, 0.77, 1.00)
    colors[clr.SliderGrabActive]       = ImVec4(0.26, 0.98, 0.85, 1.00)
    colors[clr.Button]                 = ImVec4(0.26, 0.98, 0.85, 0.40)
    colors[clr.ButtonHovered]          = ImVec4(0.26, 0.98, 0.85, 1.00)
    colors[clr.ButtonActive]           = ImVec4(0.06, 0.98, 0.82, 1.00)
    colors[clr.Header]                 = ImVec4(0.26, 0.98, 0.85, 0.31)
    colors[clr.HeaderHovered]          = ImVec4(0.26, 0.98, 0.85, 0.80)
    colors[clr.HeaderActive]           = ImVec4(0.26, 0.98, 0.85, 1.00)
    colors[clr.Separator]              = colors[clr.Border]
    colors[clr.SeparatorHovered]       = ImVec4(0.10, 0.75, 0.63, 0.78)
    colors[clr.SeparatorActive]        = ImVec4(0.10, 0.75, 0.63, 1.00)
    colors[clr.ResizeGrip]             = ImVec4(0.26, 0.98, 0.85, 0.25)
    colors[clr.ResizeGripHovered]      = ImVec4(0.26, 0.98, 0.85, 0.67)
    colors[clr.ResizeGripActive]       = ImVec4(0.26, 0.98, 0.85, 0.95)
    colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
    colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.81, 0.35, 1.00)
    colors[clr.TextSelectedBg]         = ImVec4(0.26, 0.98, 0.85, 0.35)
    colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
    colors[clr.WindowBg]               = ImVec4(0.06, 0.06, 0.06, 0.94)
    colors[clr.ChildWindowBg]          = ImVec4(1.00, 1.00, 1.00, 0.00)
    colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
    colors[clr.ComboBg]                = colors[clr.PopupBg]
    colors[clr.Border]                 = ImVec4(0.43, 0.43, 0.50, 0.50)
    colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
    colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
    colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
    colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
    colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
    colors[clr.CloseButton]            = ImVec4(0.41, 0.41, 0.41, 0.50)
    colors[clr.CloseButtonHovered]     = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.CloseButtonActive]      = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
    colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
    colors[clr.ModalWindowDarkening]   = ImVec4(0.80, 0.80, 0.80, 0.35)
end

function redTheme()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
    local ImVec2 = imgui.ImVec2
    style.Alpha = 1
    style.ChildWindowRounding = 15
    style.WindowRounding = 15
    style.GrabRounding = 15
    style.GrabMinSize = 20
    style.FrameRounding = 10

    colors[clr.FrameBg] = ImVec4(0.48, 0.16, 0.16, 0.54)
    colors[clr.FrameBgHovered] = ImVec4(0.98, 0.26, 0.26, 0.40)
    colors[clr.FrameBgActive] = ImVec4(0.98, 0.26, 0.26, 0.67)
    colors[clr.TitleBg] = ImVec4(0.04, 0.04, 0.04, 1.00)
    colors[clr.TitleBgActive] = ImVec4(0.48, 0.16, 0.16, 1.00)
    colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
    colors[clr.CheckMark] = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.SliderGrab] = ImVec4(0.88, 0.26, 0.24, 1.00)
    colors[clr.SliderGrabActive] = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.Button] = ImVec4(0.98, 0.26, 0.26, 0.40)
    colors[clr.ButtonHovered] = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.ButtonActive] = ImVec4(0.98, 0.06, 0.06, 1.00)
    colors[clr.Header] = ImVec4(0.98, 0.26, 0.26, 0.31)
    colors[clr.HeaderHovered] = ImVec4(0.98, 0.26, 0.26, 0.80)
    colors[clr.HeaderActive] = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.Separator] = colors[clr.Border]
    colors[clr.SeparatorHovered] = ImVec4(0.75, 0.10, 0.10, 0.78)
    colors[clr.SeparatorActive] = ImVec4(0.75, 0.10, 0.10, 1.00)
    colors[clr.ResizeGrip] = ImVec4(0.98, 0.26, 0.26, 0.25)
    colors[clr.ResizeGripHovered] = ImVec4(0.98, 0.26, 0.26, 0.67)
    colors[clr.ResizeGripActive] = ImVec4(0.98, 0.26, 0.26, 0.95)
    colors[clr.TextSelectedBg] = ImVec4(0.98, 0.26, 0.26, 0.35)
    colors[clr.Text] = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled] = ImVec4(0.50, 0.50, 0.50, 1.00)
    colors[clr.WindowBg] = ImVec4(0.06, 0.06, 0.06, 0.94)
    colors[clr.ChildWindowBg] = ImVec4(1.00, 1.00, 1.00, 0.00)
    colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
    colors[clr.ComboBg] = colors[clr.PopupBg]
    colors[clr.Border] = ImVec4(0.43, 0.43, 0.50, 0.50)
    colors[clr.BorderShadow] = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.MenuBarBg] = ImVec4(0.14, 0.14, 0.14, 1.00)
    colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.53)
    colors[clr.ScrollbarGrab] = ImVec4(0.31, 0.31, 0.31, 1.00)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.41, 0.41, 0.41, 1.00)
    colors[clr.ScrollbarGrabActive] = ImVec4(0.51, 0.51, 0.51, 1.00)
    colors[clr.CloseButton] = ImVec4(0.41, 0.41, 0.41, 0.50)
    colors[clr.CloseButtonHovered] = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.CloseButtonActive] = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
    colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
    colors[clr.PlotHistogram] = ImVec4(0.90, 0.70, 0.00, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
    colors[clr.ModalWindowDarkening] = ImVec4(0.80, 0.80, 0.80, 0.35)
end

function ShowHelpMarker(desc)
    imgui.TextDisabled('(?)')
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
        imgui.PushTextWrapPos(450.0)
        imgui.TextUnformatted(desc)
        imgui.PopTextWrapPos()
        imgui.EndTooltip()
    end
end

function GetTheme()
  if HLcfg.main.theme == 1 then apply_custom_style()
  elseif HLcfg.main.theme == 2 then lightBlue()
  elseif HLcfg.main.theme == 3 then redTheme()
	elseif HLcfg.main.theme == 4 then mintTheme()
  elseif HLcfg.main.theme == 5 then blackTheme()
  elseif HLcfg.main.theme == 6 then yellowTheme()
  elseif HLcfg.main.theme == 7 then darkRedTheme() end
end
GetTheme()


function cmd()
act = not act
if act then
			sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}activated. ', -1)
			else
		sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}deactivated.', -1)
		activate = false
	end
	end
function timer()
	while true do
		wait(0)

		if shour ~= nil then
			wait(1000)

			ssec = ssec + 1

			if ssec == 60 then
				ssec = 0
				smin = smin + 1
			end

			if smin == 60 then
				smin = 0
				shour = shour + 1
			end
		end
	end
	end

function imgui.OnDrawFrame()
  if main_window_state.v then
		Servak()
    local xPos, yPos = getScreenResolution()
local sameline = 230
imgui.SetNextWindowPos(imgui.ImVec2(xPos-400, yPos-300), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5))
imgui.SetNextWindowSize(imgui.ImVec2(510, 295), 1)




imgui.Begin(u8'�������� ����� | '..servak..' | Version: 28.07.2020 (00:00)', main_window_state, imgui.WindowFlags.NoResize or imgui.WindowFlags.NoCollapse or imgui.WindowFlags.NoSavedSettings)
imgui.BeginChild('left', imgui.ImVec2(123, 0), true)
if servak == 'SUPREME' then
if imgui.Selectable(u8"��� SUPREME", menu == 228) then menu = 228 end
end
if imgui.Selectable(u8"���������", menu == 1) then menu = 1 end
if imgui.Selectable(u8"�������", menu == 2) then menu = 2 end
if imgui.Selectable(u8"������", menu == 9) then menu = 9 end
if imgui.Selectable(u8"�������. �����", menu == 6) then menu = 6 end
if imgui.Selectable(u8"�����", menu == 7) then menu = 7 end
if imgui.Selectable(u8"����. �����", menu == 3) then menu = 3 end
if imgui.Selectable(u8"����� ����", menu == 4) then menu = 4 end
imgui.EndChild()
imgui.SameLine()


imgui.BeginChild('right', imgui.ImVec2(0, 0), true)
if menu == 1 then
        imgui.Text(u8'���� �����:')
        if imgui.InputText('##1', buffer, imgui.SameLine()) then
            HLcfg.main.jtext = string.format('%s', tostring(buffer.v))
        end
        ShowHelpMarker(u8'���� ����� ����� ������� ���������.', imgui.SameLine())

        imgui.Text(u8'���������:')
        if imgui.InputText('##2', buffermenu, imgui.SameLine()) then
            sampUnregisterChatCommand(HLcfg.main.activation)
            HLcfg.main.activation = buffermenu.v
            sampRegisterChatCommand(tostring(HLcfg.main.activation), function() main_window_state.v = not main_window_state.v end)
        end
        ShowHelpMarker(u8'������� ��� ��������� �������.', imgui.SameLine())



                      imgui.NewLine()




elseif menu == 2 then
if imgui.Checkbox(u8'�������� ���� � �������� � ������� �����', imgui.ImBool(HLcfg.main.nClear)) then
            HLcfg.main.nClear = not HLcfg.main.nClear
        end
        ShowHelpMarker(u8'� ������ � ������ ����� ��������� ������� �� ����� ����', imgui.SameLine())

        if imgui.Checkbox(u8'����������� � 5 ��������', imgui.ImBool(HLcfg.main.max5)) then
            HLcfg.main.max5 = not HLcfg.main.max5
        end
        ShowHelpMarker(u8'��������� ������� � ������ ����� ������ 5 ��������.', imgui.SameLine())

        if imgui.Checkbox(u8'��������� ���� � �����', tags) then
            pStSet = sampGetServerSettingsPtr()
            if tags.v then
                mem.setint8(pStSet + 56, 0)
                for i = 1, 2048 do
                    if sampIs3dTextDefined(i) then
                        local text, color, posX, posY, posZ, distance, ignoreWalls, player, vehicle = sampGet3dTextInfoById(i)
                        for ii = 1, #allFams do if text:match(string.format('.+%s', allFams[tonumber(ii)])) then sampDestroy3dText(i) end end
                    end
                end
            else
                mem.setint8(pStSet + 56, 1)
            end
        end
        ShowHelpMarker(u8'��������� ��� � �������� ����� ��� �������.\n(�������� ����� �� �������� - �� ���������)', imgui.SameLine())
		----
			if imgui.Checkbox(u8'�������� ������� "�� �����"', imgui.ImBool(HLcfg.main.delnoflood)) then
            HLcfg.main.delnoflood = not HLcfg.main.delnoflood
        end
		 ShowHelpMarker(u8'������ ������ ������ ������� ������� "�� �����" � ����. �� �������� "������ �� N"', imgui.SameLine())

			if imgui.Checkbox(u8'�������� /buybiz', imgui.ImBool(HLcfg.main.perebind)) then
            HLcfg.main.perebind = not HLcfg.main.perebind
        end
		 ShowHelpMarker(u8'��� ������� ������ "N", � ��� ���������� ������� "/buybiz".', imgui.SameLine())
elseif menu == 3 then
imgui.Text(u8'������')
        if imgui.InputText('##3', keybuff, imgui.SameLine()) then
            HLcfg.main.key = string.format('%s', tostring(keybuff.v))
        end
        ShowHelpMarker(u8'������ �� ������� ����� ����������� �����.', imgui.SameLine())

        if imgui.Checkbox(u8'��������� ����� �� ������', imgui.ImBool(HLcfg.main.onkey)) then
          HLcfg.main.onkey = not HLcfg.main.onkey
        end

        if imgui.Button(u8'������� �����') then showCaptcha() end
        if imgui.Button(u8'�������� ����������', imgui.SameLine()) then for i = 1, 400 do sampTextdrawDelete(i) end end
		imgui.Text(u8'/hbuybiz - �������� ����� �� �������.')
	elseif menu == 228 then
		imgui.Text(u8'������� ���� �������� ������ �� Arizona Supreme.')
		imgui.Text(u8'/tpm - �������� �� �����')
		imgui.Text(u8'/tpc - �������� �� ��������')
    imgui.Text(u8'/ltp � /ltpc (���� �������� ������� Supreme)')
		imgui.Text(u8'/eda - ��������� ���������� �����.')
		imgui.Text(u8'/hp - ��������� ���������� ��������.')
elseif menu == 6 then
		imgui.Text(u8'�������� �� �����: /ltp')
				imgui.Text(u8'�������� �� ���������: /ltpc')
	imgui.Text(u8'������������: /spawn')
		imgui.Text(u8'�������: /suicide')
				imgui.Text(u8'��������� ������: /slap')
	imgui.Checkbox(u8'��������', col)
				 ShowHelpMarker(u8'������� ��������� ������ �������, ���������� �� ����� ����/�������.', imgui.SameLine())
				 imgui.Checkbox(u8'�����������', antinefludi)
				 ShowHelpMarker(u8'��������� �������� ������� N �� PayDay', imgui.SameLine())
				 imgui.Checkbox(u8'����������� �2', antinefluditwo)
				 ShowHelpMarker(u8'��������� �������� ������� N �� ������� ��� ���������/������ ���������', imgui.SameLine())
				 imgui.Checkbox(u8'����������� N', avtonazatie)
				 ShowHelpMarker(u8'������������� ������ N ����� PayDay.', imgui.SameLine())
				 imgui.Checkbox(u8'����������� N �2', avtonazatietwo)
				 ShowHelpMarker(u8'������������� ������ N ����� ��������� ������� ���/������/�������� ���������.', imgui.SameLine())
				 imgui.Checkbox(u8'����������� ENTER', aenter)
				 ShowHelpMarker(u8'������������� ������ ENTER ����� ����� 5 ����.', imgui.SameLine())
				 if avtonazatie.v then
				 imgui.Text(u8'�������� �����������')
				 ShowHelpMarker(u8'��������� �������� ����� �������� N ����� PayDay.', imgui.SameLine())
				 imgui.SliderInt(u8"##1", waitavtonazatie, 0, 1000)
				 imgui.SameLine(15, nil)
				 imgui.NewLine()
			 end
			 if avtonazatietwo.v then
				 imgui.Text(u8'�������� ����������� �2')
				 ShowHelpMarker(u8'��������� �������� ����� �������� N ����� ��������� ������� ��� ���������/������ ���������.', imgui.SameLine())
				 imgui.SliderInt(u8"##2", waitavtonazatietwo, 0, 1000)
				 imgui.SameLine(15, nil)
				 imgui.NewLine()
			 end
			 if aenter.v then
				 imgui.Text(u8'�������� ����������� ENTER')
				 ShowHelpMarker(u8'��������� �������� ����� �������� ENTER ����� ����, ��� �� �������� 5 ����.', imgui.SameLine())
				 imgui.SliderInt(u8"##3", waitaenter, 0, 1000)
				 imgui.SameLine(15, nil)
				 imgui.NewLine()
			 end
			 if (avtonazatie.v or avtonazatietwo.v or aenter.v) then
					imgui.Separator()
				 end
elseif menu == 9 then
if	imgui.Checkbox(u8'������� ������', imgui.ImBool(HLcfg.main.oopsTimer)) then
	HLcfg.main.oopsTimer = not HLcfg.main.oopsTimer
end
        ShowHelpMarker(u8'������� ������ ������� ���������� ����� � �� ������� ��� (�������� �� ����������� �������� Arizona)', imgui.SameLine())
        if imgui.Checkbox(u8'����� �������/��������� � �.�', imgui.ImBool(HLcfg.main.utime)) then
					HLcfg.main.utime = not HLcfg.main.utime
			end
        ShowHelpMarker(u8'����� � ��� �� ������� �� ������ ��� ��� ������', imgui.SameLine())

        if imgui.Checkbox(u8'����� ������� ������ ���������', imgui.ImBool(HLcfg.main.ptime)) then
            HLcfg.main.ptime = not HLcfg.main.ptime
        end
        ShowHelpMarker(u8'����� � ��� �� ������� ������ ����� ����� ��� ��� ������', imgui.SameLine())

        if imgui.Checkbox(u8'��������� � ��� ����� ������� ���������', imgui.ImBool(HLcfg.main.HLmsg)) then
            HLcfg.main.HLmsg = not HLcfg.main.HLmsg
        end
        ShowHelpMarker(u8'����� ������� ���� ��� ������� ����� � ��� ���� �����,\n������� ����� ������� �� ������� "����� ���������"', imgui.SameLine())


        if imgui.Checkbox(u8'��������� ����� �� �����', imgui.ImBool(HLcfg.main.texttime)) then HLcfg.main.texttime = not HLcfg.main.texttime end
        ShowHelpMarker(u8'��������� � ���� ����� �����, �� ������� �� ������� ���������.', imgui.SameLine())

        if imgui.Checkbox(u8'������ ������� � PayDay', imgui.ImBool(HLcfg.main.resetpd)) then HLcfg.main.resetpd = not HLcfg.main.resetpd end
        ShowHelpMarker(u8'���� ��������, �� ������ ����� �������� �� ������� ������.\n���� ���������, �� ������ ����� �������� �� ������� �������� �����.', imgui.SameLine())

elseif menu == 7 then
			if imgui.Checkbox(u8'��������� ����� � ������.', imgui.ImBool(HLcfg.main.bindstyle)) then
            HLcfg.main.bindstyle = not HLcfg.main.bindstyle
        end
        ShowHelpMarker(u8'��� ������� ������ "X" � ��� ��������� ����� ������. ������ ��� TwinTurbo � PD �����.', imgui.SameLine())


			if imgui.Checkbox(u8'�������� ��������', imgui.ImBool(HLcfg.main.bindphone)) then
            HLcfg.main.bindphone = not HLcfg.main.bindphone
        end
        ShowHelpMarker(u8'��� ������� ������ "P" � ��� ��������� �������.', imgui.SameLine())
			if imgui.Checkbox(u8'����', imgui.ImBool(HLcfg.main.bindsbiv)) then
            HLcfg.main.bindsbiv = not HLcfg.main.bindsbiv
        end
        ShowHelpMarker(u8'��� ������� ������ "Q" � ��� ���������� ������� /anims 1', imgui.SameLine())
		--
					if imgui.Checkbox(u8'������� ����', imgui.ImBool(HLcfg.main.clearchat)) then
            HLcfg.main.clearchat = not HLcfg.main.clearchat
        end
        ShowHelpMarker(u8'��� ������� ������ "O" � ��� ��������� ���.', imgui.SameLine())
		---
							if imgui.Checkbox(u8'��������/�������� ������', imgui.ImBool(HLcfg.main.bindlock)) then
            HLcfg.main.bindlock = not HLcfg.main.bindlock
        end
        ShowHelpMarker(u8'��� ������� ������ "L" � ��� ���������/��������� ������.', imgui.SameLine())
		---
		----
       imgui.Text(u8'����������� �������: "/findihouse" - "/fh".')

elseif menu == 4 then
if HLcfg.main.theme ~= 1 then apply_custom_style() end
        if imgui.Button(u8'����������') then HLcfg.main.theme = 1; apply_custom_style() end
        GetTheme()
        if HLcfg.main.theme ~= 2 then lightBlue() end
        if imgui.Button(u8'������-�����', imgui.SameLine()) then HLcfg.main.theme = 2; lightBlue() end
        GetTheme()
				if HLcfg.main.theme ~= 3 then redTheme() end
        if imgui.Button(u8'�������', imgui.SameLine()) then HLcfg.main.theme = 3; redTheme() end
        GetTheme()
				if HLcfg.main.theme ~= 4 then mintTheme() end
				if imgui.Button(u8'������', imgui.SameLine()) then HLcfg.main.theme = 4; mintTheme() end
				GetTheme()
				if HLcfg.main.theme ~= 5 then blackTheme() end
				if imgui.Button(u8'������') then HLcfg.main.theme = 5; blackTheme() end
				GetTheme()
				if HLcfg.main.theme ~= 6 then yellowTheme() end
				if imgui.Button(u8'Ƹ����', imgui.SameLine()) then HLcfg.main.theme = 6; yellowTheme() end
				GetTheme()
				if HLcfg.main.theme ~= 7 then darkRedTheme() end
				if imgui.Button(u8'Ҹ���-�������', imgui.SameLine()) then HLcfg.main.theme = 7; darkRedTheme() end
				GetTheme()
end
imgui.EndChild()



imgui.End()
end
end


function samp.onUnoccupiedSync()
if sync1.v then return false end
end
function samp.onPlayerSync()
if sync2.v then return false end
end

function samp.onVehicleSync()
if sync3.v then return false end
end

function samp.onPassengerSync()
if  sync4.v then return false end
end

  function kol()
	for _, pedv in ipairs(getAllChars()) do
	if doesCharExist(pedv) then
		if pedv ~= playerPed then
	   setCharCollision(pedv, false)
	end
end
end
end

function randomFloat(lower, greater)
    return lower + math.random()  * (greater - lower);
end
--math.random(239.8,240.5) + nextPos, math.random(128.8,math.random(130.5,math.random(127.3,math.random(131.1,132.1))))

function showCaptcha()
    removeTextdraws()
    t = t + 1
    sampTextdrawCreate(t, "LD_SPAC:white", math.random(220,math.random(219.1,math.random(221.5,218.1))), math.random(120,math.random(121.2,119.1)))
    sampTextdrawSetLetterSizeAndColor(t, 0, 6.5, 0x80808080)
    sampTextdrawSetBoxColorAndSize(t, 1, 0xFF1A2432, 380, 0.000000)

    t = t + 1
    sampTextdrawCreate(t, "LD_SPAC:white", math.random(225,math.random(226.1,224.4)), math.random(125,126.1))
    sampTextdrawSetLetterSizeAndColor(t, 0, 5.5, 0x80808080)
    sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, 375, 0.000000)
    nextPos = math.random(-30,math.random(-28,math.random(-27,-33)));

    math.randomseed(os.time())
    for i = 1, 4 do
        a = math.random(0, 9)
        table.insert(captchaTable, a)
        captcha = captcha..a
    end
       -- sampTextdrawCreate(t, "usebox", math.random(239.8,math.random(240.5,math.random(241.1,238.9))) + nextPos, math.random(128.8,math.random(130.5,math.random(127.3,math.random(131.1,math.random(132.1,129.1))))))
    for i = 0, 4 do
        nextPos = nextPos + math.random(30,29.5)
        t = t + 1
        sampTextdrawCreate(t, "usebox", math.random(240,math.random(239,math.random(241,math.random(240.5,math.random(242.5,math.random(239.6,math.random(237.6,math.random(235,230)))))))) + nextPos, math.random(130,math.random(131,math.random(132,math.random(133,math.random(134,math.random(135,math.random(129,math.random(128,math.random(124,126.6))))))))))
        sampTextdrawSetLetterSizeAndColor(t, 0, 4.5, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF1A2432, math.random(30,31), 25.000000)
        sampTextdrawSetAlign(t, 2)
        if i < 4 then GenerateTextDraw(captchaTable[i + 1], math.random(240,239) + nextPos, math.random(130,131), 3 + i * 10)
        else GenerateTextDraw(0, 240 + nextPos, math.random(130,math.random(129,131)), 3 + i * 10) end
    end
    captchaTable = {}
    sampShowDialog(8812, '{F89168}�������� �� ������', '{FFFFFF}������� {C6FB4A}5{FFFFFF} ��������, �������\n����� �� {C6FB4A}�����{FFFFFF} ������.', '�������', '������', 1)
    captime = os.clock()
end

function removeTextdraws()
  if t > 0 then
    for i = 1, t do sampTextdrawDelete(i) end
    t = 0
    captcha = ''
    captime = 0
  end
end

function GenerateTextDraw(id, PosX, PosY)
  if id == 0 then
    t = t + 1
    sampTextdrawCreate(t, "LD_SPAC:white", PosX - 5, PosY + 7)
    sampTextdrawSetLetterSizeAndColor(t, 0, 3, 0x80808080)
    sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX+5, 0.000000)
  elseif id == 1 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then offsetX = 3; offsetBX = 15 else offsetX = -3; offsetBX = -15; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX - offsetX, PosY)
        sampTextdrawSetLetterSizeAndColor(t, 0, 4.5, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  elseif id == 2 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then offsetX = -8; offsetY = 7 offsetBX = 15 else offsetX = 6; offsetY = 25 offsetBX = -15; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX - offsetX, PosY + offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, 0.8, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  elseif id == 3 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 0.8; offsetY = 7 else size = 1; offsetY = 25 end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX+10, PosY+offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, 1, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-15, 0.000000)
    end
  elseif id == 4 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 1.8; offsetX = -10; offsetY = 0 offsetBX = 10 else size = 2; offsetX = -10; offsetY = 25 offsetBX = 15; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX - offsetX, PosY + offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, size, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  elseif id == 5 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 0.8; offsetX = 8; offsetY = 7 offsetBX = -15 else size = 1; offsetX = -10; offsetY = 25 offsetBX = 15; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX - offsetX, PosY + offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, size, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  elseif id == 6 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 0.8; offsetX = 7.5; offsetY = 7 offsetBX = -15 else size = 1; offsetX = -10; offsetY = 25 offsetBX = 10; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX - offsetX, PosY + offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, size, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  elseif id == 7 then
    t = t + 1
    sampTextdrawCreate(t, "LD_SPAC:white", PosX - 13, PosY + 7)
    sampTextdrawSetLetterSizeAndColor(t, 0, 3.75, 0x80808080)
    sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX+5, 0.000000)
  elseif id == 8 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 0.8; offsetY = 7 else size = 1; offsetY = 25 end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX+10, PosY+offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, 1, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-10, 0.000000)
    end
  elseif id == 9 then
    for i = 0, 1 do
        t = t + 1
        if i == 0 then size = 0.8; offsetY = 6; offsetBX = 10; else size = 1; offsetY = 25; offsetBX = 15; end
        sampTextdrawCreate(t, "LD_SPAC:white", PosX+10, PosY+offsetY)
        sampTextdrawSetLetterSizeAndColor(t, 0, 1, 0x80808080)
        sampTextdrawSetBoxColorAndSize(t, 1, 0xFF759DA3, PosX-offsetBX, 0.000000)
    end
  end
end



function main()
  if not isSampfuncsLoaded() or not isSampLoaded() then return end
  while not isSampAvailable() do wait(100) end
	ip, port = sampGetCurrentServerAddress()
autoupdate("http://nogalesarz.000webhostapp.com/helperlovli/autoupdate.json", '['..string.upper(thisScript().name)..']: ', "https://vk.com/kizzn")
	sampRegisterChatCommand('ltp', function()
		if not isCharOnFoot(playerPed)
			then return
	sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}�� ������ ���������� �� � ������.', -1)
	end
	lua_thread.create(function()
		if tp then return end

			blip, blipX, blipY, blipZ = getTargetBlipCoordinatesFixed()
			if blip then
				sync = true
				charPosX, charPosY, charPosZ = getCharCoordinates(playerPed)
				local distan = getDistanceBetweenCoords3d(blipX, blipY, charPosZ, charPosX, charPosY, charPosZ)
				if distan < 25 then return setCharCoordinates(playerPed, blipX, blipY, blipZ) end
				tp = true
				tpTime = os.clock()
			end
		end)
	end)
		 sampRegisterChatCommand('ltpc', function()
	if not isCharOnFoot(playerPed)
			then return
sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}�� ������ ���������� �� � ������.', -1)
	end
	lua_thread.create(function()
		if tp then return end
			blip, blipX, blipY, blipZ = SearchMarker()
			if blip then
				sync = true
				charPosX, charPosY, charPosZ = getCharCoordinates(playerPed)
				local distan = getDistanceBetweenCoords3d(blipX, blipY, charPosZ, charPosX, charPosY, charPosZ)
				if distan < 25 then return setCharCoordinates(playerPed, blipX, blipY, blipZ) end
				tp = true
				tpTime = os.clock()
			end
		end)
	end)
	lua_thread.create(function()
		while true do wait(0)
			if tp then
				ncharPosX, ncharPosY, ncharPosZ = getCharCoordinates(playerPed)
				local distanc = getDistanceBetweenCoords3d(blipX, blipY, charPosZ, charPosX, charPosY, charPosZ)
				if getDistanceBetweenCoords3d(blipX, blipY, charPosZ, charPosX, charPosY, charPosZ) > 7 then
				printStringNow('Teleport...', 500)
					vectorX = blipX - charPosX
					vectorY = blipY - charPosY
					vectorZ = blipZ - charPosZ
					local vec = vector(vectorX, vectorY, vectorZ)
					vec:normalize()
					charPosX = charPosX + vec.x * 7
					charPosY = charPosY + vec.y * 7
					charPosZ = charPosZ + vec.z * 7

					sendOnfootSync(charPosX, charPosY, charPosZ)
					sendOnfootSync(charPosX, charPosY, charPosZ + 55)

			return true
				end

					sendOnfootSync(charPosX, charPosY, charPosZ)
					sendOnfootSync(charPosX, charPosY, charPosZ + 55)
					setCharCoordinates(playerPed, blipX, blipY, blipZ)
					wait(1000)
					tp = false
					printStringNow('~y~please wait~W~...',5000)
					printStringNow('~W~Teleported. ~P~Lovell Fam', 4000)
					 sync = false
				end
			end
		--end
	end)
  	        sampRegisterChatCommand('fh', function(num)
                if num ~= nil then
                sampSendChat('/findihouse '..num)
                end
            end)

  sampRegisterChatCommand('dtpc', dtpc)
	  sampRegisterChatCommand('eda', EdaCheck)
			  sampRegisterChatCommand('hp', HpCheck)
	  sampRegisterChatCommand('spawn', SpawnCheck)
			  sampRegisterChatCommand('slap', pozhope)
			  sampRegisterChatCommand('suicide', umri)
  sampRegisterChatCommand('hbuybiz', hbuybiz)


  sampRegisterChatCommand('wh',wh)
  -----


 -- sampRegisterChatCommand('ltp', ltp)
  sampRegisterChatCommand(tostring(HLcfg.main.activation), function() main_window_state.v = not main_window_state.v end)
  notf.addNotification('���������: /' ..HLcfg.main.activation, 5, 1)
  notf.addNotification('������ ������� ��������!', 5, 1)
  sampAddChatMessage('{DA70D6}[�������� �����] {ffffff}by lovell fam {ffffff}| {ffffff}������: 28.07.2020 (00:00) ', -1)
	sampRegisterChatCommand('tpm', Newttp)
    sampRegisterChatCommand('tpc', Newtpc)
	font1 = renderCreateFont("Tahoma", 17 , 5)
	lua_thread.create(timer)
  while true do wait(0)
             if os.clock() - timert > 0 and synct then
              timert, tpCountt = 0, 0
              synct = true
              sampForceOnfootSync()
              synct = true
            end
            if tpt then
                if getDistanceBetweenCoords3d(blipXt, blipYt, blipZt, charPosXt, charPosYt, charPosZt) > 15 then
                    vectorXt = blipXt - charPosXt
                    vectorYt = blipYt - charPosYt
                    vectorZt = blipZt - charPosZt
                    local vec = vector(vectorXt, vectorYt, vectorZt)
                    vec:normalize()
                    charPosXt = charPosXt + vec.x * 15
                    charPosYt = charPosYt + vec.y * 15
                    charPosZt = charPosZt + vec.z * 15
                    sendOnfootSync(charPosXt, charPosYt, charPosZt)
                    sendOnfootSync(charPosXt, charPosYt, charPosZt)

                    tpCountt = tpCountt + 1
                    if tpCountt == 2 then
                    sampForceOnfootSync()
                    sendOnfootSync(charPosXt, charPosYt, charPosZt)
                    printStringNow('~y~iSetPlayerPos: ~g~true', 434)
                      sendOnfootSync(charPosXt, charPosYt + 25, charPosZt - 70)
                      wait(1828)
                       sampForceOnfootSync()
                       sendOnfootSync(charPosXt, charPosYt, charPosZt)
                       wait(2000)
                       sendOnfootSync(charPosXt, charPosYt, charPosZt)
                      sampForceOnfootSync()
                      wait(228)
                      tpCountt = 0
                      printStringNow('~y~iSetPlayerPos: ~r~false', 434)
                      sendOnfootSync(charPosXt, charPosYt, charPosZt)
                    end
                else
                  sendOnfootSync(charPosXt, charPosYt, charPosZt + 2)
                  printStringNow('~y~iSetPlayerPos: ~b~false', 898)
                  sendOnfootSync(charPosXt, charPosYt + 55, charPosZt + 3)
                  synct = true
                  sendOnfootSync(charPosXt, charPosYt, charPosZt + 2)
                  wait(1000)
                  printStringNow("~>~ ~p~successful ~s~teleport ~<~",2500)
                  addOneOffSound(0.0, 0.0, 0.0, 1139)
                  synct = true
                  sendOnfootSync(charPosXt, charPosYt, charPosZt + 2)
                  synct = false
                  wait(75)
                  setCharCoordinates(playerPed, blipXt, blipYt, blipZt + 1)
                  tpt = false
				  lockPlayerControl(false)
                  timert, tpCountt = 0, 0
                end
            end
    keybuff.v = string.upper(keybuff.v)
    if wasKeyPressed(vkeys.name_to_id(keybuff.v, false)) and HLcfg.main.onkey and not sampIsChatInputActive() and not sampIsDialogActive() then showCaptcha() end
    local result, button, list, input = sampHasDialogRespond(8812)
    if result then
      if button == 1 then
        if input == captcha..'0' then sampAddChatMessage(string.format('{DA70D6}[�������� �����] {ffffff}������ ����� [%.3f]. ����� ���� '..captcha..'0', os.clock() - captime), -1)
        elseif input ~= captcha..'0' then sampAddChatMessage(string.format('{DA70D6}[�������� �����] {ffffff}�������� ����� [%.3f] ('..captcha..'0|'..input..')', os.clock() - captime), -1) end
      end
      removeTextdraws()
			if captcha == nil then
				showCaptcha()
			end
    end
if HLcfg.main.bindstyle then
	if testCheat("x") and not sampIsCursorActive() then
        sampSendChat("/style")
	end
	end
	if HLcfg.main.bindphone then
	if testCheat("P") and not sampIsCursorActive() then
        sampSendChat("/phone")
    end
	end
	if HLcfg.main.bindsbiv then
		if testCheat("Q") and not isCharInAnyCar(PLAYER_PED) then
 sampSendChat("/anims 1")
 end
 end
 --
 	if HLcfg.main.perebind then
		if testCheat("N") and not sampIsChatInputActive() then
 sampSendChat("/buybiz")
 end
 end
	if HLcfg.main.clearchat then
 		if testCheat("O") and not sampIsChatInputActive() then
 sampAddChatMessage("",-1)
  sampAddChatMessage("",-1)
   sampAddChatMessage("",-1)
    sampAddChatMessage("",-1)
	 sampAddChatMessage("",-1)
	  sampAddChatMessage("",-1)
	   sampAddChatMessage("",-1)
	    sampAddChatMessage("",-1)
		sampAddChatMessage("",-1)
		 sampAddChatMessage("",-1)
		  sampAddChatMessage("",-1)
		   sampAddChatMessage("",-1)
		    sampAddChatMessage("",-1)
			 sampAddChatMessage("",-1)
 end
 end
 ---
 	if HLcfg.main.bindlock then
		if testCheat("l") then
			sampSendChat("/lock")
    end
	end

	if col.v then kol() end
    if sampIsDialogActive() and sampGetDialogCaption():find('�������� �� ������') then
      if HLcfg.main.nClear then sampSetCurrentDialogEditboxText(string.gsub(sampGetCurrentDialogEditboxText(), '[^1234567890]','')) end
      if HLcfg.main.max5 then
        local text = sampGetCurrentDialogEditboxText()
        if #text > 5 then sampSetCurrentDialogEditboxText(text:sub(1, 5)) end
      end
    end
    imgui.Process = main_window_state.v
  end
  wait(-1)
end

function nameTagOn()
local pStSet = sampGetServerSettingsPtr()
NTdist = mem.getfloat(pStSet + 39)
NTwalls = mem.getint8(pStSet + 47)
NTshow = mem.getint8(pStSet + 56)
mem.setfloat(pStSet + 39, 1488.0)
mem.setint8(pStSet + 47, 0)
mem.setint8(pStSet + 56, 1)
end

function nameTagOff()
local pStSet = sampGetServerSettingsPtr()
mem.setfloat(pStSet + 39, NTdist)
mem.setint8(pStSet + 47, NTwalls)
mem.setint8(pStSet + 56, NTshow)
end

function wh()
act = not act
if act then
nameTagOn()
printStringNow('WH on!', 1000)
else
nameTagOff()
printStringNow('WH off!', 1000)
end
end


function dtpc()
lua_thread.create(function()
        if tpt then return sampAddChatMessage('��� ��������������', -1) end

            blipt, blipXt, blipYt, blipZt = SearchMarker(blipXt, blipYt, blipZt)
            if blipt then
                synct = true
                charPosXt, charPosYt, charPosZt = getCharCoordinates(playerPed)
                local distant = getDistanceBetweenCoords3d(blipXt, blipYt, charPosZt, charPosXt, charPosYt, charPosZt)
                if distant < 15 then return setCharCoordinates(playerPed, blipXt, blipYt, blipZt) end
                tpt = true
                setCharCoordinates(playerPed, blipXt, blipYt, blipZt)
                wait(2000)
                printStringNow('~y~iSetPlayerPos: ~p~true', 4000)
            end
        end)
    end

function hbuybiz()
	showCaptcha()
end

function getTargetBlipCoordinatesFixed() -- snippet by Azller Lollison
    local bool, x, y, z = getTargetBlipCoordinates(); if not bool then return false end
    requestCollision(x, y); loadScene(x, y, z)
    local bool, x, y, z = getTargetBlipCoordinates()
    return bool, x, y, z
end
-- Helper Lovli by vk.com/kiZzn. Special for lovellfam.
function samp.onPlayerChatBubble() if tags.v then return false end end

function samp.onCreate3DText(id, color, position, distance, testLOS, attachedPlayerId, attachedVehicleId, text)
  if time ~= nil and HLcfg.main.ptime then
    local _, pid = sampGetPlayerIdByCharHandle(playerPed)
      pname = sampGetPlayerNickname(pid)
    for pizda in text:gmatch('[^\n\r]+') do
      pizda = pizda:match('{FFFFFF}��������: {AFAFAF}(.+)')
      if pizda ~= nil and pizda ~= pname then return sampAddChatMessage(string.format('{DA70D6}[������ �����] {ffffff}��� ������ ������� {20B2AA}%s {ffffff}[%.3f]', pizda, os.clock() - time), -1) end
    end
    for pizda in text:gmatch('[^\n\r]+') do
      pizda = pizda:match('{73B461}��������: {FFFFFF}(.+)')
      if pizda ~= nil and pizda ~= pname then return sampAddChatMessage(string.format('{DA70D6}[������ �����] {ffffff}������ ������ ������� {20B2AA}%s {ffffff}[%.3f]', pizda, os.clock() - time), -1) end
    end
  end
end

function samp.onSendPlayerSync(data)
  if data.weapon == 128 and enable then return false end
  if HLcfg.main.floodN and data.weapon == 128 and not payday then return false
  elseif HLcfg.main.floodN and data.weapon == 128 and payday then payday = false end
end

function samp.onShowDialog(dialogId, style, title, button1, button2, text)
  if act then
 if text:find("������� �����") then
		shour, smin, ssec = string.match(text, "������� �����: \t{345690}(.+):(.+):(.+)")
		activate = true

		end
	end
  if title:find('�������� �� ������') then
    if not HLcfg.main.resetpd then
      time = os.clock()
      reset()
    end
  end
  if text:find('/findihouse ID') and HLcfg.main.autofind then
    for line in text:gmatch('[^\n\r]+') do
      if line:find('��� ���������� ����� ���� {1EA3CC}���������%.{FFFFFF}') then
        if #forFind == 1 then
          sampSendChat('/findihouse '..forFind[1])
          table.remove(forFind, 1)
          sampAddChatMessage('{DA70D6}[�������� �����] {ffffff}������ ���� ���. ����� �����������!', -1)
        elseif #forFind > 1 then
          sampAddChatMessage('{DA70D6}[�������� �����] {ffffff}������� {DA70D6}'..#forFind..' {ffffff}�����, ���� ���������!', -1)
          sampSendChat('/findihouse '..forFind[1])
        elseif #forFind == 0 then
          sampAddChatMessage('{DA70D6}[�������� �����] {ffffff}����� � ���� ���!', -1)
        end
        return true
      end
      local hid = line:match('.+%. ���        ID: {C9B931}(.+){FFFFFF}    %[{A9FF14}������{FFFFFF}]')
      if hid ~= nil then table.insert(forFind, hid) end
    end
  end

if aenter.v and Lovlya1 == true then
		lua_thread.create(function()
			wait(0)
			while true do wait(0)
		textsumfive = sampGetCurrentDialogEditboxText()
		if #textsumfive == 5 then
			wait (waitaenter.v)
			sampCloseCurrentDialogWithButton(1)
			aenter.v = false
			Lovlya1 = false
			break
	end
	end
	end)
	end
end



function onWindowMessage(msg, wparam, lparam)
  if msg == 0x100 or msg == 0x101 then
    if wparam == vkeys.VK_ESCAPE and main_window_state.v and not isPauseMenuActive() then
      consumeWindowMessage(true, false)
      main_window_state.v = false
    end
  end
		if antinefludi.v or antinefluditwo.v then
		if msg == 0x100 and param == 0x4E then
		consumeWindowMessage()
	end
	end
end

function reset()
  lua_thread.create(function()
    wait(4000)
    time = nil
  end)
end

function onScriptTerminate(script, quitGame)
  if script == thisScript() then
    inicfg.save(HLcfg, "HelperLovli")
  end
end

function onQuitGame()
  inicfg.save(HLcfg, "HelperLovli")
end

function GetRedMarkerCoords() -- snippet by SR_team, slightly modified
    for i = 0, 31 do
        local markerPtr = 0xC7F168 + i * 56
        --local markerPtr = 0xC7DEC8 + i * 160
        local x = representIntAsFloat(readMemory(markerPtr + 0, 4, false))
        local y = representIntAsFloat(readMemory(markerPtr + 4, 4, false))
        local z = representIntAsFloat(readMemory(markerPtr + 8, 4, false))
        if x ~= 0.0 or y ~= 0.0 or z ~= 0.0 then
            requestCollision(x, y); loadScene(x, y, z);
            return x, y, z
        end
    end
    return 0, 0, 0
end

function samp.onShowDialog(id, style, title, b1, b2, text)
    if title:find("�������� �� ������") then
		Lovlya1 = true
    did = id
    start = os.clock()
    end
end

function samp.onSendDialogResponse(id, but, lis, input)
if id == did then
    time = os.clock() - start
    time1 = string.format("%.3f", time)
if oopsTimer.v then
	sampAddChatMessage("{EE82EE}[������ �����]{FFFFFF} �� ����� ��: {FB8900}"..time1.." {FFFFFF} ������. ���� ����� ����: {FB8900}"..input.."{FFFFFF} !", -1)
end
end
end

function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

 -- function samp.onCreate3DText(i, clr, pos, distance, ignoreWalls, playerId, vehicleId, textd)
 -- if (textd:find("*** ��� ��������� ***") or textd:find("������ ���������") or textd:find("*** �������� ��������� ***")) then
-- 	 key = 'U'
--  end
-- end
-- ��� ���������� ���� ��
--              		if avtonazatietwo.v and (textd:find("*** ��� ��������� ***") or textd:find("������ ���������")) then
--              			lua_thread.create(function()
--              				antinefludi.v = false
--              				antinefluditwo.v = false
--              			wait (waitavtonazatietwo.v)
--              			local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
--              			    local data = allocateMemory(68)
--              			    sampStorePlayerOnfootData(myId, data)
--              			    setStructElement(data, 36, 1, 128, false)
--              			    sampSendOnfootData(data)
--              			    freeMemory(data)



local carfortp = {
    478, 430, 446, 452, 453, 473, 472, 454, 484, 493, 595, 543, 605, 538, 433, 408, 601, 582, 546, 578, 554, 413, 422, 407, 456, 407, 570
}

function samp.onSendPlayerSync()
    if tp then return false end
end


function tpc(arg)
 if tp then return end

 local posX, posY, posZ = getCharCoordinates(PLAYER_PED)
 local res, x, y, z = SearchMarker(posX, posY, posZ)
 if not res then return sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}�������� �� ������', -1) end

 arg = tonumber(arg)
 if arg ~= nil then
     requestCollision(x, y)
     tp = true
     lua_thread.create(tp_proccess_started, arg, x, y, z)

     return
 end

 local veh = FindVehicleForTp()
 if veh == -1 then return sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}������ �� �������', -1) end

 requestCollision(x, y)
 loadScene(x, y, z)

 tp = true
 lua_thread.create(tp_proccess_started, veh, x, y, z)
end

function Newttp(arg)
	if ip == '176.32.37.28' and port == 7777 then ttp(arg) end
 end

function EdaCheck()
	if ip == '176.32.37.28' and port == 7777 then SupremeEda() end
end

function HpCheck()
	if ip == '176.32.37.28' and port == 7777 then SupremeHp() end
end

function SupremeEda()
	sampSendClickTextdraw(106)
end

function SupremeHp()
	sampSendClickTextdraw(104)
end

 function SpawnCheck(arg)
 	if  ip == '185.169.134.3' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.4' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.43' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.44' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.45' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.5' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.59' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.61' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.107' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.109' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.166' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.171' and port == 7777 then musoralohi()
	elseif ip == '185.169.134.172' and port == 7777 then musoralohi()
	end
  end

function Newtpc(arg)
	if ip == '176.32.37.28' and port == 7777 then tpc(arg) end
 end

function ttp(arg)

    if tp then return end


    local res, x, y, z = getTargetBlipCoordinates()
    if not res then return sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}����� �� �������', -1) end

    arg = tonumber(arg)
    if arg ~= nil then
        requestCollision(x, y)
        loadScene(x, y, z)

        tp = true
        lua_thread.create(tp_proccess_started, arg, x, y, getGroundZFor3dCoord(x, y, 999.0))

        return
    end

    local veh = FindVehicleForTp()
    if veh == -1 then return sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}������ �� �������', -1) end

    requestCollision(x, y)
    loadScene(x, y, z)

    tp = true
    lua_thread.create(tp_proccess_started, veh, x, y, getGroundZFor3dCoord(x, y, 999.0))
end

function tp_proccess_started(veh, x, y, z)
    local res, hand = sampGetCarHandleBySampVehicleId(veh)
    if not res then
        tp = false

        sampAddChatMessage('{DA70D6}[�������� �����] {FFFFFF}�������� �������', -1)

        return
    end

    lockPlayerControl(true)

    local cx, cy, cz = getCharCoordinates(PLAYER_PED)

    wait(200)

    sendOnfootSync(cx, cy, cz, veh)
    setCharCoordinates(PLAYER_PED, x, y, z)
    sampForceOnfootSync()
    sendOnfootSync(x, y, z, veh)
    wait(1500)
    --sendPassengerSync(x, y, z)
    sendOnfootSync(x, y, z, 0)

    tp = false
    sampAddChatMessage('{DA70D6}[!] {FFFFFF}�������� ��������', -1)
    lockPlayerControl(false)
end

function FindVehicleForTp()
    local x, y, z = getCharCoordinates(PLAYER_PED)
    for i = 0, 2000 do
        local res, veh = sampGetCarHandleBySampVehicleId(i)
        if res then
            local cx, cy, cz = getCarCoordinates(veh)
            if getDistanceBetweenCoords3d(x, y, z, cx, cy, cz) < 200.0 then
                 for j = 1, #carfortp do
                     if isCarModel(veh, carfortp[j]) then
                        return i
                     end
                 end
            end
        end
    end
    return -1
end

function sendPassengerSync(x, y, z)
    local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
    local data = allocateMemory(63)
    sampStorePlayerPassengerData(myId, data)
    setStructFloatElement(data, 12, x, false)
    setStructFloatElement(data, 16, y, false)
    setStructFloatElement(data, 20, z, false)
    setStructElement(data, 2, 1, 1, false)
    sampSendPassengerData(data)
    freeMemory(data)
end

function sendOnfootSync(x, y, z, veh)
    local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
    local data = allocateMemory(68)
    sampStorePlayerOnfootData(myId, data)
    setStructElement(data, 37, 1, 3, false)
    setStructFloatElement(data, 6, x, false)
    setStructFloatElement(data, 10, y, false)
    setStructFloatElement(data, 14, z, false)
    setStructElement(data, 62, 2, veh, false)
    sampSendOnfootData(data)
    freeMemory(data)
end

function SearchMarker(posX, posY, posZ)
  local ret_posX = 0.0
  local ret_posY = 0.0
  local ret_posZ = 0.0
  local isFind = false
  for id = 0, 31 do
      local MarkerStruct = 0
      MarkerStruct = 0xC7F168 + id * 56
      local MarkerPosX = representIntAsFloat(readMemory(MarkerStruct + 0, 4, false))
      local MarkerPosY = representIntAsFloat(readMemory(MarkerStruct + 4, 4, false))
      local MarkerPosZ = representIntAsFloat(readMemory(MarkerStruct + 8, 4, false))
      if MarkerPosX ~= 0.0 or MarkerPosY ~= 0.0 or MarkerPosZ ~= 0.0 then
              ret_posX = MarkerPosX
              ret_posY = MarkerPosY
              ret_posZ = MarkerPosZ
              isFind = true
      end
  end
  return isFind, ret_posX, ret_posY, ret_posZ
end

function musoralohi()
	setInteriorVisible(2)
end

function umri()
setCharHealth(PLAYER_PED, 0)
end

function pozhope()
	local x, y, z = getCharCoordinates(PLAYER_PED)
	setCharCoordinates(PLAYER_PED, x, y, z + 10.0)
end

function samp.onServerMessage(color, text)
if HLcfg.main.delnoflood then
	if text:find('�� �����') then
	return false
end
---
if avtonazatie.v and (text:match('��� ��������� PayDay �� ������ �������� ������� 20 �����.') or text:match('__________���������� ���__________')) then
lua_thread.create(function()
	wait(0)
	antinefludi.v = false
	antinefluditwo.v = false
wait (waitavtonazatie.v)
local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
		local data = allocateMemory(68)
		sampStorePlayerOnfootData(myId, data)
		setStructElement(data, 36, 1, 128, false)
		sampSendOnfootData(data)
		freeMemory(data)
	end)
end
end
---
  if text:find('%[���������] {FFFFFF}�� ��������� �������� ����� ��� ����� ����������� ���.') then
    if #forFind > 0 then
      local x, y, z = getCharCoordinates(playerPed)
      local x1, y1, z1 = GetRedMarkerCoords()
      houses[forFind[1]] = getDistanceBetweenCoords3d(x, y, z, x1, y1, z1)
      table.remove(forFind, 1)
      if #forFind > 0 then sampSendChat('/findihouse '..forFind[1]) end
      if #forFind == 0 then hfind = 1 end
    end --
    if #forFind == 0 and hfind == 1 then
        for k,v in spairs(houses, function(t,a,b) return t[b] > t[a] end) do
          hfind = 2
          forFind, houses = {}, {}
          sampAddChatMessage('{DA70D6}[�������� �����] {ffffff}��������� ��� - '..k..'. ����� ����������� �� �����!', -1)
          sampSendChat('/findihouse '..k)
          return
        end
    end
    if hfind == 2 then
      hfind = 0
      return false
    end
  end

  if text:find('__________________________________') and color == 1941201407 or
    text:find('��� ��������� PayDay �� ������ �������� ������� 20 �����.') and color == -10270721 then
      if HLcfg.main.resetpd then
        time = os.clock()
        reset()
      end
      payday = true
  end
  if text:find('%[����������] {FFFFFF}����������! ������ ���� ������ ���!') and color == 1941201407 and time ~= nil then
    if HLcfg.main.HLmsg then
      if HLcfg.main.texttime then sampSendChat(u8:decode(string.format('%s [%.3f]', HLcfg.main.jtext, time)))
      else sampSendChat(u8:decode(HLcfg.main.jtext)) end
    end
  end
  if text:find('%[����������] {FFFFFF}����������! ������ ���� ��� ���!') and color == 1941201407 and time ~= nil then
    if HLcfg.main.HLmsg then
      if HLcfg.main.texttime then sampSendChat(u8:decode(string.format('%s [%.3f]', HLcfg.main.jtext, time)))
      else sampSendChat(u8:decode(HLcfg.main.jtext)) end
    end
  end
  if text:find('����������! ������ ���� ��� ���') and time ~= nil then
    if HLcfg.main.HLmsg then
      if HLcfg.main.texttime then sampSendChat(u8:decode(string.format('%s [%.3f]', HLcfg.main.jtext, time)))
      else sampSendChat(u8:decode(HLcfg.main.jtext)) end
    end
  end

  if text:find('>> {FF6347} ������ ������ ������������� �������� ��� ���. �������� ����� ������ {FFFFFF}>>{FF6347} /setspawn!') and time ~= nil and HLcfg.main.utime then time = os.clock() - time end
end
--- � ����� ����� --- --- VIP 777 ---
function samp.onSetPlayerPos(p)
	if sync then
		timer = os.clock()
		return false
	end
end

function samp.onSendPlayerSync(data)
	if tp then return false end
end

function sendOnfootSync(x, y, z)
    local data = samp_create_sync_data('player')
	data.position = {x, y, z}
	data.moveSpeed = {0.899999, 0.899999, -0.899999}
	data.send()
end

function getTargetBlipCoordinatesFixed()
    local bool, x, y, z = getTargetBlipCoordinates(); if not bool then return false end
    requestCollision(x, y); loadScene(x, y, z)
    local bool, x, y, z = getTargetBlipCoordinates()
    return bool, x, y, z
end

function SearchMarker(posX, posY, posZ)
    local ret_posX = 0.0
    local ret_posY = 0.0
    local ret_posZ = 0.0
    local isFind = false
    for id = 0, 31 do
        local MarkerStruct = 0
        MarkerStruct = 0xC7F168 + id * 56
        local MarkerPosX = representIntAsFloat(readMemory(MarkerStruct + 0, 4, false))
        local MarkerPosY = representIntAsFloat(readMemory(MarkerStruct + 4, 4, false))
        local MarkerPosZ = representIntAsFloat(readMemory(MarkerStruct + 8, 4, false))
        if MarkerPosX ~= 0.0 or MarkerPosY ~= 0.0 or MarkerPosZ ~= 0.0 then
            ret_posX = MarkerPosX
            ret_posY = MarkerPosY
            ret_posZ = MarkerPosZ
            isFind = true
        end
    end
    return isFind, ret_posX, ret_posY, ret_posZ
end

function samp_create_sync_data(sync_type, copy_from_player)
    local ffi = require 'ffi'
    local sampfuncs = require 'sampfuncs'
    -- from SAMP.Lua
    local raknet = require 'samp.raknet'
    --require 'samp.synchronization'

    copy_from_player = copy_from_player or true
    local sync_traits = {
        player = {'PlayerSyncData', raknet.PACKET.PLAYER_SYNC, sampStorePlayerOnfootData},
        vehicle = {'VehicleSyncData', raknet.PACKET.VEHICLE_SYNC, sampStorePlayerIncarData},
        passenger = {'PassengerSyncData', raknet.PACKET.PASSENGER_SYNC, sampStorePlayerPassengerData},
        aim = {'AimSyncData', raknet.PACKET.AIM_SYNC, sampStorePlayerAimData},
        trailer = {'TrailerSyncData', raknet.PACKET.TRAILER_SYNC, sampStorePlayerTrailerData},
        unoccupied = {'UnoccupiedSyncData', raknet.PACKET.UNOCCUPIED_SYNC, nil},
        bullet = {'BulletSyncData', raknet.PACKET.BULLET_SYNC, nil},
        spectator = {'SpectatorSyncData', raknet.PACKET.SPECTATOR_SYNC, nil}
    }
    local sync_info = sync_traits[sync_type]
    local data_type = 'struct ' .. sync_info[1]
    local data = ffi.new(data_type, {})
    local raw_data_ptr = tonumber(ffi.cast('uintptr_t', ffi.new(data_type .. '*', data)))
    -- copy player's sync data to the allocated memory
    if copy_from_player then
        local copy_func = sync_info[3]
        if copy_func then
            local _, player_id
            if copy_from_player == true then
                _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
            else
                player_id = tonumber(copy_from_player)
            end
            copy_func(player_id, raw_data_ptr)
        end
    end
    -- function to send packet
    local func_send = function()
        local bs = raknetNewBitStream()
        raknetBitStreamWriteInt8(bs, sync_info[2])
        raknetBitStreamWriteBuffer(bs, raw_data_ptr, ffi.sizeof(data))
        raknetSendBitStreamEx(bs, sampfuncs.HIGH_PRIORITY, sampfuncs.UNRELIABLE_SEQUENCED, 1)
        raknetDeleteBitStream(bs)
    end
    -- metatable to access sync data and 'send' function
    local mt = {
        __index = function(t, index)
            return data[index]
        end,
        __newindex = function(t, index, value)
            data[index] = value
        end
    }
    return setmetatable({send = func_send}, mt)
end

function autoupdate(json_url, prefix, url)
  local dlstatus = require('moonloader').download_status
	local prefix = '{DA70D6}[�������� �����] {FFFFFF}'
  local json = getWorkingDirectory() .. '\\'..thisScript().name..'-version.json'
  if doesFileExist(json) then os.remove(json) end
  downloadUrlToFile(json_url, json,
    function(id, status, p1, p2)
      if status == dlstatus.STATUSEX_ENDDOWNLOAD then
        if doesFileExist(json) then
          local f = io.open(json, 'r')
          if f then
            local info = decodeJson(f:read('*a'))
            updatelink = info.updateurl
            updateversion = info.latest
            f:close()
            os.remove(json)
            if updateversion ~= thisScript().version then
              lua_thread.create(function(prefix)
                local dlstatus = require('moonloader').download_status
                local color = -1
                sampAddChatMessage((prefix..'���������� ����������. ������� ������:{629eee} '..thisScript().version..' {DA70D6}|{d5dedd} ����� ������:{629eee} '..updateversion), color)
                wait(250)
                downloadUrlToFile(updatelink, thisScript().path,
                  function(id3, status1, p13, p23)
                    if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
                      print(string.format('��������� %d �� %d.', p13, p23))
                    elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
                      print('�������� ���������� ���������.')
                      sampAddChatMessage((prefix..'���������� ���������!'), color)
                      goupdatestatus = true
                      lua_thread.create(function() wait(500) thisScript():reload() end)
                    end
                    if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                      if goupdatestatus == nil then
                        sampAddChatMessage((prefix..'���������� ������ ��������. �������� ���������� ������..'), color)
                        update = false
                      end
                    end
                  end
                )
                end, prefix
              )
            else
              update = false
              print('v'..thisScript().version..': ���������� �� ���������.')
            end
          end
        else
          print('v'..thisScript().version..': �� ���� ��������� ����������. ���������� ����� ��� ��������� ������� �� '..url)
          update = false
        end
      end
    end
  )
  while update ~= false do wait(100) end
end

function download_handler(id, status, p1, p2)
  if stop_downloading then
    stop_downloading = false
    download_id = nil
    print('�������� ��������.')
    return false
  end
end

function Servak()
	if ip == '176.32.37.28' and port == 7777 then servak = 'SUPREME'
elseif  ip == '185.169.134.3' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.4' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.43' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.44' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.45' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.5' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.59' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.61' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.107' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.109' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.166' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.171' and port == 7777 then servak = 'ARIZONA RP'
	elseif ip == '185.169.134.172' and port == 7777 then servak = 'ARIZONA RP'
end
end

function ServakAutoEnter()
	if ip == '176.32.37.28' and port == 7777 then servakautoenter = '23045'
elseif  ip == '185.169.134.3' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.4' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.43' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.44' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.45' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.5' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.59' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.61' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.107' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.109' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.166' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.171' and port == 7777 then servakautoenter = '8869'
elseif ip == '185.169.134.172' and port == 7777 then servakautoenter = '8869'
end
end

function samp.onServerMessage(color, text)
	if avtonazatie.v and (text:match('��� ��������� PayDay �� ������ �������� ������� 20 �����.') or text:match('__________���������� ���__________')) then
	lua_thread.create(function()
		wait(0)
		antinefludi.v = false
		antinefluditwo.v = false
	wait (waitavtonazatie.v)
	local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
			local data = allocateMemory(68)
			sampStorePlayerOnfootData(myId, data)
			setStructElement(data, 36, 1, 128, false)
			sampSendOnfootData(data)
			freeMemory(data)
		end)
	end
end

  function samp.onCreate3DText(i, clr, pos, distance, ignoreWalls, playerId, vehicleId, textd)
		if antinefluditwo.v then
			lua_thread.create(function()
				repeat wait(0) until textd:find("*** ��� ��������� ***") or textd:find("������ ���������")
		 if i == 1024 and textd:find("*** ��� ��������� ***") or textd:find("������ ���������") then
			 antinefluditwo.v = false
		 end
	 end)
	 end
			 if avtonazatietwo.v and (textd:find("*** ��� ��������� ***") or textd:find("������ ���������")) then
				 lua_thread.create(function()
					 antinefludi.v = false
					 antinefluditwo.v = false
				 wait (waitavtonazatietwo.v)
				 local _, myId = sampGetPlayerIdByCharHandle(PLAYER_PED)
						 local data = allocateMemory(68)
						 sampStorePlayerOnfootData(myId, data)
						 setStructElement(data, 36, 1, 128, false)
						 sampSendOnfootData(data)
						 freeMemory(data)
	 end)
	 end
	 end


              function samp.onShowDialog(dialogID, style, title, button1, button2, txt)
	 if aenter.v and dialogID == 23045 or dialogID == 8869 then
			 lua_thread.create(function()
				 wait(0)
				 while true do wait(0)
			 textsumfive = sampGetCurrentDialogEditboxText()
			 if #textsumfive == 5 then
				 wait (waitaenter.v)
				 sampCloseCurrentDialogWithButton(1)
				 break
			end
			end
			end)
		 end
end
